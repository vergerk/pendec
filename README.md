# pendec

Pendec (pour Pendu éducatif) est une réalisation bénévole Open Source reprenant le jeu du pendu tout en étant orienté "pédagogie". Ce jeu a deux objectifs : 

  * Permettre aux joueurs d’apprendre tout en s’amusant. Le joueur peut se challengerou défier un autre joueur et progresser grâce au système de score. 
  * Améliorer son niveau culturel avec des paliers de difficultés pour chaque thème.  