/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entite;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jojod
 */
@Entity
@Table(name = "definition")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Definition.findAll", query = "SELECT d FROM Definition d")
    , @NamedQuery(name = "Definition.findByIdDefinition", query = "SELECT d FROM Definition d WHERE d.idDefinition = :idDefinition")
    , @NamedQuery(name = "Definition.findByMot", query = "SELECT d FROM Definition d WHERE d.mot = :mot")
    , @NamedQuery(name = "Definition.findByDefinition", query = "SELECT d FROM Definition d WHERE d.definition = :definition")
    , @NamedQuery(name = "Definition.findByThemeAndNiveau", query = "SELECT d FROM Definition d WHERE d.idNiveau = :idNiveau AND d.idTheme = :idTheme")
    , @NamedQuery(name = "Definition.findByUrl", query = "SELECT d FROM Definition d WHERE d.url = :url")})
public class Definition implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idDefinition")
    private Integer idDefinition;
    @Basic(optional = false)
    @Column(name = "mot")
    private String mot;
    @Basic(optional = false)
    @Column(name = "definition")
    private String definition;
    @Basic(optional = false)
    @Column(name = "url")
    private String url;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDefinition")
    private Collection<Score> scoreCollection;
    @JoinColumn(name = "idNiveau", referencedColumnName = "idNiveau")
    @ManyToOne(optional = false)
    private Niveau idNiveau;
    @JoinColumn(name = "idTheme", referencedColumnName = "idTheme")
    @ManyToOne(optional = false)
    private Theme idTheme;

    public Definition()
    {
    }

    public Definition(Integer idDefinition)
    {
        this.idDefinition = idDefinition;
    }

    public Definition(Integer idDefinition, String mot, String definition, String url)
    {
        this.idDefinition = idDefinition;
        this.mot = mot;
        this.definition = definition;
        this.url = url;
    }

    public Integer getIdDefinition()
    {
        return idDefinition;
    }

    public void setIdDefinition(Integer idDefinition)
    {
        this.idDefinition = idDefinition;
    }

    public String getMot()
    {
        return mot;
    }

    public void setMot(String mot)
    {
        this.mot = mot;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    @XmlTransient
    public Collection<Score> getScoreCollection()
    {
        return scoreCollection;
    }

    public void setScoreCollection(Collection<Score> scoreCollection)
    {
        this.scoreCollection = scoreCollection;
    }

    public Niveau getNiveau()
    {
        return idNiveau;
    }

    public void setNiveau(Niveau idNiveau)
    {
        this.idNiveau = idNiveau;
    }

    public Theme getTheme()
    {
        return idTheme;
    }

    public void setTheme(Theme idTheme)
    {
        this.idTheme = idTheme;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (idDefinition != null ? idDefinition.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Definition)) {
            return false;
        }
        Definition other = (Definition) object;
        if ((this.idDefinition == null && other.idDefinition != null) || (this.idDefinition != null && !this.idDefinition.equals(other.idDefinition))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "Entite.Definition[ idDefinition=" + idDefinition + " ]";
    }

}
