/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entite;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jojod
 */
@Entity
@Table(name = "niveau")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Niveau.findAll", query = "SELECT n FROM Niveau n")
    , @NamedQuery(name = "Niveau.findByIdNiveau", query = "SELECT n FROM Niveau n WHERE n.idNiveau = :idNiveau")
    , @NamedQuery(name = "Niveau.findByLibelle", query = "SELECT n FROM Niveau n WHERE n.libelle = :libelle")})
public class Niveau implements Serializable {

    private static final long serialVersionUID = 1L; //test
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idNiveau")
    private Integer idNiveau;
    @Basic(optional = false)
    @Column(name = "libelle")
    private String libelle;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idNiveau")
    private Collection<Definition> definitionCollection;

    public Niveau() {
    }

    public Niveau(Integer idNiveau) {
        this.idNiveau = idNiveau;
    }

    public Niveau(Integer idNiveau, String libelle) {
        this.idNiveau = idNiveau;
        this.libelle = libelle;
    }

    public Integer getIdNiveau() {
        return idNiveau;
    }

    public void setIdNiveau(Integer idNiveau) {
        this.idNiveau = idNiveau;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @XmlTransient
    public Collection<Definition> getDefinitionCollection() {
        return definitionCollection;
    }

    public void setDefinitionCollection(Collection<Definition> definitionCollection) {
        this.definitionCollection = definitionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNiveau != null ? idNiveau.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Niveau)) {
            return false;
        }
        Niveau other = (Niveau) object;
        if ((this.idNiveau == null && other.idNiveau != null) || (this.idNiveau != null && !this.idNiveau.equals(other.idNiveau))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entite.Niveau[ idNiveau=" + idNiveau + " ]";
    }
    
}
