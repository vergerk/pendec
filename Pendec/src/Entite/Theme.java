/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entite;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jojod
 */
@Entity
@Table(name = "theme")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Theme.findAll", query = "SELECT t FROM Theme t")
    , @NamedQuery(name = "Theme.findByIdTheme", query = "SELECT t FROM Theme t WHERE t.idTheme = :idTheme")
    , @NamedQuery(name = "Theme.findByLibelle", query = "SELECT t FROM Theme t WHERE t.libelle = :libelle")})
public class Theme implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTheme")
    private Integer idTheme;

    @Basic(optional = false)
    @Column(name = "libelle")
    private String libelle;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTheme")
    private Collection<Score> scoreCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTheme")
    private Collection<Definition> definitionCollection;

    public Theme()
    {
    }

    public Theme(Integer idTheme)
    {
        this.idTheme = idTheme;
    }

    public Theme(Integer idTheme, String libelle)
    {
        this.idTheme = idTheme;
        this.libelle = libelle;
    }

    public Integer getIdTheme()
    {
        return idTheme;
    }

    public void setIdTheme(Integer idTheme)
    {
        this.idTheme = idTheme;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    @XmlTransient
    public Collection<Score> getScoreCollection()
    {
        return scoreCollection;
    }

    public void setScoreCollection(Collection<Score> scoreCollection)
    {
        this.scoreCollection = scoreCollection;
    }

    @XmlTransient
    public Collection<Definition> getDefinitionCollection()
    {
        return definitionCollection;
    }

    public void setDefinitionCollection(Collection<Definition> definitionCollection)
    {
        this.definitionCollection = definitionCollection;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (idTheme != null ? idTheme.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Theme)) {
            return false;
        }
        Theme other = (Theme) object;
        if ((this.idTheme == null && other.idTheme != null) || (this.idTheme != null && !this.idTheme.equals(other.idTheme))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "Entite.Theme[ idTheme=" + idTheme + " ]";
    }

}
