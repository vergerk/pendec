/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pendec;

/**
 * Classe pour passer des objets dans un composant graphique avec un toString personnalisé
 * @author VERGER Khevin
 */
public class ItemListe
{

    private Object obj;
    private String toStringOverr;

    public ItemListe(Object obj, String toStringOverr)
    {
        this.obj = obj;
        this.toStringOverr = toStringOverr;
    }

    public Object getObj()
    {
        return obj;
    }

    @Override
    public String toString()
    {
        return toStringOverr;
    }

}
