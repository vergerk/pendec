/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pendec;

import ui.forms.FormPrincipale;

/**
 * Lancement de la fenêtre principale
 * @author VERGER Khevin
 */
public class Pendec
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        new FormPrincipale().setVisible(true);
    }

}
