/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pendec;

import Entite.Niveau;
import Entite.Score;
import Entite.Theme;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import metier.logique.ServicesNiveau;
import metier.logique.ServicesScore;
import metier.logique.ServicesTheme;
import util.JpaUtil;

/**
 * Classe contenant les propriétés communes à tout le projet
 * @author VERGER Khevin
 */
public class Communs
{

    private static List<ImageIcon> listImgPendu = new ArrayList<ImageIcon>();
    private static List<Niveau> listNiveaux = new ArrayList<Niveau>();
    private static List<Theme> listThemes = new ArrayList<Theme>();

    private static int score;
    private static String resultat;

    public static List<ImageIcon> getListImgPendu()
    {
        return listImgPendu;
    }

    public static String getResultat()
    {
        return resultat;
    }

    public static void setResultat(String resultat)
    {
        Communs.resultat = resultat;
    }

    public static int getScore()
    {
        return score;
    }

    public static void setScore(int score)
    {
        Communs.score = score;
    }

    public static List<Niveau> getListNiveaux()
    {
        return listNiveaux;
    }

    public static List<Theme> getListThemes()
    {
        return listThemes;
    }

    public enum e_valeurRetour
    {
        valide,
        annule
    }

    public static String nvlleLigne()
    {
        return System.getProperty("line.separator");
    }

    /**
     * Initialise les classes utilisées dans tout le programme
     */
    public static void initClasses()
    {
        PeupleDbDerby();

        JpaUtil.initEntityManagerFactory();

        listNiveaux = new ServicesNiveau().listeNiveaux();
        listThemes = new ServicesTheme().listeThemes();

        if (listImgPendu.size() == 0) {
            //File f = new File("D:\\Users\\VERGER Khevin\\Documents\\OneDrive - Association Cesi Viacesi mail\\CESI\\#Projets\\Java-Net\\pendec\\Pendec\\build\\classes\\Ressources\\penduImg");
            File f = new File("build/classes/Ressources/penduImg");
            
            if (!f.exists()) // Cas ou on execute de la clé : 
            {
                f = new File("./Ressources/penduImg");
            }
          
            System.out.println(f); //  vérification en debug du chemin
            
            if (f.exists() && f.listFiles().length > 0) {
                for (File image : f.listFiles()) {
                    listImgPendu.add(new ImageIcon(image.getPath()));
                }
            }
        }

        JpaUtil.destroyEntityManagerFactory();
    }

    /**
     * Affiche les 10 derniers scores, triés du plus grand au plus petit score
     * @return Liste de Score
     */
    public static List<Score> Get10LastScore()
    {
        JpaUtil.initEntityManagerFactory();

        List<Score> r = new ServicesScore().Affiche10lastScore();

        JpaUtil.destroyEntityManagerFactory();

        return r;
    }

    /**
     * Peuple la base de données JavaDb Derby
     * @return Indique si le peuplement s'est bien déroulé
     */
    public static boolean PeupleDbDerby()
    {
        boolean etatChargement = false;
        /*
        String scriptSql = ChargeFichier(new File("build/classes/Ressources/dbDerby.sql"));

        if (scriptSql.length() > 0) {
            //String driver = "org.apache.derby.jdbc.EmbeddedDriver";
            String dbName = "pendec";
            String connectionURL = "jdbc:derby:" + dbName + ";create=true";

            try {
                //Class.forName(driver);
                //Class.forName("");

                Connection conn = DriverManager.getConnection(connectionURL);

                Statement stmt = conn.createStatement();
                stmt.executeUpdate(scriptSql);

                etatChargement = true;

            }
            catch (SQLException ex) {
                Logger.getLogger(Communs.class.getName()).log(Level.SEVERE, null, ex);

                etatChargement = false;
            }
        }
         */
        return etatChargement;
    }

    /**
     * Charge le fichier spécifié dans une chaine de caractère
     * @author Stephane Nicoll - Infonet FUNDP
     * @return Contenu du fichier
     * @version 0.1
     */
    private static String ChargeFichier(File f)
    {
        String r = "";

        if (f != null && f.exists()) {
            try {
                BufferedInputStream in = new BufferedInputStream(new FileInputStream(f));
                StringWriter out = new StringWriter();
                int b;
                while ((b = in.read()) != -1) {
                    out.write(b);
                }
                out.flush();
                out.close();
                in.close();
                r = out.toString();
            }
            catch (IOException ie) {
                ie.printStackTrace();
            }
        }

        return r;
    }
}
