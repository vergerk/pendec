CREATE TABLE  joueur (
  idJoueur INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  pseudo varchar(50) NOT NULL,
  score int NOT NULL,
  PRIMARY KEY (idJoueur)
);

CREATE TABLE niveau (
  idNiveau INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  libelle varchar(50) NOT NULL,
  PRIMARY KEY (idNiveau)
);

CREATE TABLE theme (
  idTheme INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  libelle varchar(50) NOT NULL,
  PRIMARY KEY (idTheme)
);



CREATE TABLE definition
( idDefinition INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  mot varchar(30) NOT NULL,
  definition varchar(250) NOT NULL,
  url varchar(250) NOT NULL,
  idTheme int NOT NULL,
  idNiveau int NOT NULL,
  PRIMARY KEY (idDefinition)
 -- KEY ConsTheme (idTheme),
 -- KEY ConsNiveau (idNiveau)
);

--
-- Déchargement des données de la table definition
--

INSERT INTO definition (mot, definition, url, idTheme, idNiveau) VALUES
('Roi', 'Du latin rex (roi, chef), via son accusatif regem, du verbe regere (diriger).', 'https://fr.wiktionary.org/wiki/roi', 1, 1),
('France', 'Pays des français. Situé en Europe, sa capitale est Paris. Lyon y est la plus belle ville', 'https://www.google.com/search?q=france', 2, 1),
('Musée', 'Etablissement rempli d objets rares, précieux.', 'https://www.google.com/search?q=musée+définition', 3, 1),
('Soleil', 'Etoile qui donne lumière et chaleur à la Terre. C est la boule dans le ciel visible les jours de beaux temps', 'https://www.google.com/search?q=Soleil+définition', 4, 1),
('football', 'Sport opposant deux équipes de onze joueurs, où il faut faire pénétrer un ballon rond dans les buts adverses sans utiliser les mains', 'https://www.google.com/q=football+définition', 5, 1);

INSERT INTO definition(mot, definition, url, idTheme, idNiveau) VALUES ("Chat","Petit mammifère familier à poil doux, aux yeux oblongs et brillants, à oreilles triangulaires, aux griffes rétractiles. Le chat à 4 pattes et peut être domestique ou sauvage.","https://www.google.com/search?q=definition+chat","4","1");
INSERT INTO definition(mot, definition, url, idTheme, idNiveau) VALUES ("Chien","Le chien (Canis lupus familiaris) est un mammifère carnivore, sous-espèce du loup (Canis lupus) de la famille des canidés. La femelle du chien s appelle chienne et le jeune chien s appelle chiot. ","https://fr.vikidia.org/wiki/Chien","4","1");
INSERT INTO definition(mot, definition, url, idTheme, idNiveau) VALUES ("Lion","Grand mammifère carnivore, à pelage fauve, à crinière, vivant en Afrique et en Asie.","https://www.google.com/search?q=Lion+definition","4","1");
INSERT INTO definition(mot, definition, url, idTheme, idNiveau) VALUES ("Oiseau","Animal (vertébré à sang chaud) au corps recouvert de plumes, dont les membres antérieurs sont des ailes et qui a un bec","https://www.google.com/search?q=Oiseau+definition","4","1");
INSERT INTO definition(mot, definition, url, idTheme, idNiveau) VALUES ("Arbre","Grand végétal dont la tige ligneuse se ramifie à partir d une certaine hauteur au-dessus du sol","https://www.google.com/search?q=Arbre+definition","4","1");
INSERT INTO definition(mot, definition, url, idTheme, idNiveau) VALUES ("Rivière","Cours d eau naturel de moyenne importance ou qui se jette dans un autre cours d eau.","https://www.google.com/search?q=Rivière+definition","4","1");
INSERT INTO definition(mot, definition, url, idTheme, idNiveau) VALUES ("Mer","Vaste étendue d eau salée qui couvre une grande partie de la surface du globe","https://www.google.com/search?q=Mer+definition","4","1");
INSERT INTO definition(mot, definition, url, idTheme, idNiveau) VALUES ("Souris","Petit mammifère rongeur ( spécialement la souris commune, au pelage gris).","https://www.google.com/search?q=Souris+definition","4","1");
INSERT INTO definition(mot, definition, url, idTheme, idNiveau) VALUES ("Poisson","Animal aquatique vertébré, muni de nageoires et de branchies.","https://www.google.com/search?q=Poisson+definition","4","1");
INSERT INTO definition(mot, definition, url, idTheme, idNiveau) VALUES ("Nuage","Amas de vapeur d eau condensée en fines gouttelettes maintenues en suspension dans l atmosphère","https://www.google.com/search?q=Nuage+definition","4","1");
INSERT INTO definition(mot, definition, url, idTheme, idNiveau) VALUES ("Terre","L élément solide qui supporte les êtres vivants et où poussent les végétaux","https://www.google.com/search?q=Terre+definition","4","1");
--
INSERT INTO definition(mot, definition, url, idTheme, idNiveau) VALUES ("Basketball","Le basketball ou basket-ball (mot d origine anglaise) est un sport où compètent deux équipes de cinq joueurs chacune. Ils doivent manipuler le ballon avec les mains. L objectif est d introduire le ballon dans l arceau du panier de l équipe adversaire, qui se trouve à 3,05 mètres de hauteur.","https://www.google.com/search?q=basketball+definition","5","1");


-- --------------------------------------------------------

--
-- Structure de la table niveau
--



--
-- Déchargement des données de la table niveau
--

INSERT INTO niveau (libelle) VALUES
('Junior'),
('Intermediaire'),
('1er de la classe'),
('Expert');

-- --------------------------------------------------------

--
-- Structure de la table score
--

CREATE TABLE score (
  idScore INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  score int NOT NULL,
  idNom int NOT NULL,
  idTheme int NOT NULL,
  idDefinition int NOT NULL,
  PRIMARY KEY (idScore)
 -- KEY ConsNom (idNom),
 -- KEY ConsTheme2 (idTheme),
 -- KEY ConsDef2 (idDefinition)
);

-- --------------------------------------------------------

--
-- Structure de la table theme
--


--
-- Déchargement des données de la table theme
--

INSERT INTO theme (libelle) VALUES
('Histoire'),
('Géographie'),
('Art et Littérature'),
('Science et nature'),
('Sports et loisirs');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table definition
--
ALTER TABLE definition
  ADD CONSTRAINT ConsNiveau FOREIGN KEY (idNiveau) REFERENCES niveau (idNiveau);
ALTER TABLE definition
  ADD CONSTRAINT ConsTheme FOREIGN KEY (idTheme) REFERENCES theme (idTheme);

--
-- Contraintes pour la table score
--
ALTER TABLE score
  ADD CONSTRAINT ConsDef2 FOREIGN KEY (idDefinition) REFERENCES definition (idDefinition);
ALTER TABLE score
  ADD CONSTRAINT ConsNom FOREIGN KEY (idNom) REFERENCES joueur (idJoueur);
ALTER TABLE score
  ADD CONSTRAINT ConsTheme2 FOREIGN KEY (idTheme) REFERENCES theme (idTheme);