INSERT INTO `theme`(`libelle`) VALUES ("Histoire");
INSERT INTO `theme`(`libelle`) VALUES ("Géographie");
INSERT INTO `theme`(`libelle`) VALUES ("Art et Littérature");
INSERT INTO `theme`(`libelle`) VALUES ("Science et nature");
INSERT INTO `theme`(`libelle`) VALUES ("Sports et loisirs");
INSERT INTO `niveau`(`libelle`) VALUES ("Junior");
INSERT INTO `niveau`(`libelle`) VALUES ("Intermediaire");
INSERT INTO `niveau`(`libelle`) VALUES ("1er de la classe");
INSERT INTO `niveau`(`libelle`) VALUES ("Expert");
INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("Roi","Du latin rex (roi, chef), via son accusatif regem, du verbe regere (diriger).","https://fr.wiktionary.org/wiki/roi","1","1");
INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("France","Pays des français. Situé en Europe, sa capitale est Paris. Lyon y est la plus belle ville","https://www.google.com/search?q=france","2","1");
INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("Musée","Etablissement rempli d'objets rares, précieux.","https://www.google.com/search?q=musée+définition","3","1");
INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("Soleil","Etoile qui donne lumière et chaleur à la Terre. C'est la boule dans le ciel visible les jours de beaux temps","https://www.google.com/search?q=Soleil+définition","4","1");
INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("football","Sport opposant deux équipes de onze joueurs, où il faut faire pénétrer un ballon rond dans les buts adverses sans utiliser les mains","https://www.google.com/q=football+définition","5","1");

INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("Chat","Petit mammifère familier à poil doux, aux yeux oblongs et brillants, à oreilles triangulaires, aux griffes rétractiles. Le chat à 4 pattes et peut être domestique ou sauvage.","https://www.google.com/search?q=definition+chat","4","1");
INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("Chien","Le chien (Canis lupus familiaris) est un mammifère carnivore, sous-espèce du loup (Canis lupus) de la famille des canidés. La femelle du chien s'appelle chienne et le jeune chien s'appelle chiot. ","https://fr.vikidia.org/wiki/Chien","4","1");
INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("Lion","Grand mammifère carnivore, à pelage fauve, à crinière, vivant en Afrique et en Asie. Son cri est le rugissement","https://www.google.com/search?q=lion+définition","4","1");
INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("Oiseau","Animal (vertébré à sang chaud) au corps recouvert de plumes, dont les membres antérieurs sont des ailes et qui a un bec.","https://www.google.com/search?q=definition+oiseau","4","1");
INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("Arbre","Grand végétal dont la tige ligneuse se ramifie à partir d'une certaine hauteur au-dessus du sol.","https://www.google.com/search?q=definition+arbre","4","1");
INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("Rivière","Cours d'eau naturel de moyenne importance ou qui se jette dans un autre cours d'eau.","https://www.google.com/search?q=definition+Riviere","4","1");
INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("Mer","Vaste étendue d'eau salée qui couvre une grande partie de la surface du globe.","https://www.google.com/search?q=definition+mer","4","1");
INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("Souris","Petit mammifère rongeur ( spécialement la souris commune, au pelage gris).","https://www.google.com/search?q=definition+souris","4","1");
INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("Poisson","Animal aquatique vertébré, muni de nageoires et de branchies.","https://www.google.com/search?q=definition+Poisson","4","1");
INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("Nuage","Amas de vapeur d'eau condensée en fines gouttelettes maintenues en suspension dans l'atmosphère.","https://www.google.com/search?q=definition+Nuage","4","1");
INSERT INTO `definition`(`mot`, `definition`, `url`, `idTheme`, `idNiveau`) VALUES ("Terre","Planète du système solaire habitée par l'Homme","https://www.google.com/search?q=definition+terre","4","1");



