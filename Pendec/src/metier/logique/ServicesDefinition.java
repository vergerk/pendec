/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.logique;

import Entite.Definition;
import Entite.Niveau;
import Entite.Theme;
import dao.DefinitionDao;
import util.JpaUtil;

/**
 * Services Definition
 * @author VERGER Khevin
 */
public class ServicesDefinition
{

    /**
     * Retourne une définition obtenu au hasard d'après un Niveau et un Thème
     * @param n
     * @param t
     * @return Definition au hasard
     */
    public Definition motAuHasard(Niveau n, Theme t)
    {
        JpaUtil.createEntityManager();

        Definition r = new DefinitionDao().getRandomDefinition(t, n);

        JpaUtil.closeEntityManager();

        return r;
    }
}
