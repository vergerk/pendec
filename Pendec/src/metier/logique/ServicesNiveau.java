/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.logique;

import Entite.Niveau;
import dao.NiveauDao;
import java.util.List;
import util.JpaUtil;

/**
 * Services Niveau
 * @author VERGER Khevin
 */
public class ServicesNiveau
{

    /**
     * Retourne une liste de Niveau
     * @return Liste de Niveau
     */
    public List<Niveau> listeNiveaux()
    {
        // appel de la DAO pour enregistrer le client
        JpaUtil.createEntityManager();
        List<Niveau> r = new NiveauDao().findAllNiveau();

        // obligatoire pour éviter qu'un service accède à l'entityManager partagé
        JpaUtil.closeEntityManager();

        return r;
    }
}
