/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.logique;

import Entite.Theme;
import dao.ThemeDao;
import java.util.List;
import util.JpaUtil;

/**
 * Services Thème
 * @author VERGER Khevin
 */
public class ServicesTheme
{

    /**
     * Retourne une liste de Theme
     * @return Lsite de Theme
     */
    public List<Theme> listeThemes()
    {
        // appel de la DAO pour enregistrer le client
        JpaUtil.createEntityManager();
        List<Theme> r = new ThemeDao().findAllTheme();

        // obligatoire pour éviter qu'un service accède à l'entityManager partagé
        JpaUtil.closeEntityManager();

        return r;
    }
}
