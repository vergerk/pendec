/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.logique;

import Entite.Joueur;
import dao.JoueurDao;
import util.JpaUtil;

/**
 * Services Joueur
 * @author jojod
 */
public class ServicesJoueur
{

    /**
     * Enregistre le nouveau joueur passé en paramètre
     * @param j
     */
    public void enregistrerNouveauJoueur(Joueur j)
    {
        //Appeler la DAO pour enregistrer le Client c
        // ici just ela pour les CRUD les plus basiques
        JpaUtil.createEntityManager();
        // on aurait pu ecrire :
        // EntityManager em=JpaUtil.getEntityManager();
        // em.getTransaction().begin();
        JpaUtil.getEntityManager().getTransaction().begin();
        try {
            JoueurDao dao = new JoueurDao();
            dao.createJoueur(j);
            JpaUtil.getEntityManager().getTransaction().commit();
        }
        catch (Exception ex) {
            if (JpaUtil.getEntityManager().getTransaction().isActive()) {
                JpaUtil.getEntityManager().getTransaction().rollback();
            }
        }
        JpaUtil.closeEntityManager();
    }

    /**
     * Met à jour le joueur passé en paramètre
     * @param j
     */
    public void majJoueur(Joueur j)
    {
        //Appeler la DAO pour enregistrer le Client c
        // ici just ela pour les CRUD les plus basiques
        JpaUtil.createEntityManager();
        JpaUtil.getEntityManager().getTransaction().begin();
        try {
            JoueurDao dao = new JoueurDao();
            dao.updateJoueur(j);
            JpaUtil.getEntityManager().getTransaction().commit();
        }
        catch (Exception ex) {
            if (JpaUtil.getEntityManager().getTransaction().isActive()) {
                JpaUtil.getEntityManager().getTransaction().rollback();
            }
        }
        JpaUtil.closeEntityManager();
    }

}
