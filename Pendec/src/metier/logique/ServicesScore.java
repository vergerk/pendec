/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.logique;

import Entite.Score;
import dao.ScoreDao;
import java.util.List;
import util.JpaUtil;

/**
 * Services Score
 * @author jojod
 */
public class ServicesScore
{

    /**
     * Obient les 10 derniers score
     * @return une liste de 10 scores
     */
    public List<Score> Affiche10lastScore()
    {
        // appel de la DAO pour récupérer le score
        JpaUtil.createEntityManager();
        List<Score> s = new ScoreDao().Affiche10lastScore();

        // obligatoire pour éviter qu'un service accède à l'entityManager partagé
        JpaUtil.closeEntityManager();

        return s;
    }

    /**
     * Enregistre le nouveau score passé en paramètre
     * @param s
     */
    public void enregistrerNouveauScore(Score s)
    {
        //Appeler la DAO pour enregistrer le Client c
        // ici just ela pour les CRUD les plus basiques
        JpaUtil.createEntityManager();
        // on aurait pu ecrire :
        // EntityManager em=JpaUtil.getEntityManager();
        // em.getTransaction().begin();
        JpaUtil.getEntityManager().getTransaction().begin();
        try {
            ScoreDao dao = new ScoreDao();
            dao.createScore(s);
            JpaUtil.getEntityManager().getTransaction().commit();
        }
        catch (Exception ex) {
            System.out.println("erreur : " + ex);
            if (JpaUtil.getEntityManager().getTransaction().isActive()) {
                JpaUtil.getEntityManager().getTransaction().rollback();
            }
        }

        JpaUtil.closeEntityManager();
    }
}
