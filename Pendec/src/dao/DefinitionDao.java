/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Entite.Definition;
import Entite.Niveau;
import Entite.Theme;
import java.util.List;
import javax.persistence.Query;
import util.JpaUtil;

/**
 * DAO Definition
 * @author VERGER Khevin
 */
public class DefinitionDao
{

    /**
     * Retourne une définition aléatoire d'après le thème et le niveau passé en paramètres
     * @param t
     * @param n
     * @return Definition aléatoire
     */
    public Definition getRandomDefinition(Theme t, Niveau n)
    {
        Definition r = null;

        // on obttient toutes les définitions
        Query q = JpaUtil.getEntityManager().createNamedQuery("Definition.findByThemeAndNiveau");
        q.setParameter("idTheme", t);
        q.setParameter("idNiveau", n);

        List<Definition> liste = (List<Definition>) q.getResultList();

        if (liste.size() > 0) {
            r = liste.get(
                    (int) (Math.random() * liste.size())
            );
        }

        return r;
    }
}
