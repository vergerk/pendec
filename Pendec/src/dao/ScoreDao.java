/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Entite.Score;
import java.util.List;
import util.JpaUtil;

/**
 * DAO Score
 * @author jojod
 */
public class ScoreDao
{

    /**
     * Créer le score
     * @param s
     */
    public void createScore(Score s)
    {
        JpaUtil.getEntityManager().persist(s);
    }
    
    /**
     * Pour les 10 derniers score
     * @return une liste de 10 scores
     */

    public List<Score> Affiche10lastScore()
    {
        return JpaUtil.getEntityManager().createNamedQuery("Score.find10Score").setMaxResults(10).getResultList();
    }
    

}
