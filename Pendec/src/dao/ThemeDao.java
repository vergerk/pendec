/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Entite.Theme;
import java.util.List;
import util.JpaUtil;

/**
 * DAO Thème
 * @author VERGER Khevin
 */
public class ThemeDao
{

    /**
     * Retourne une liste de thèmes
     * @return liste de thème
     */
    public List<Theme> findAllTheme()
    {
        return JpaUtil.getEntityManager().createNamedQuery("Theme.findAll").getResultList();
    }
}
