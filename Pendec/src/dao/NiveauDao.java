/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Entite.Niveau;
import java.util.List;
import util.JpaUtil;

/**
 * DAO Niveau
 * @author VERGER Khevin
 */
public class NiveauDao
{

    /**
     * Retourne une liste de Niveau
     * @return liste de Niveau
     */
    public List<Niveau> findAllNiveau()
    {
        return JpaUtil.getEntityManager().createNamedQuery("Niveau.findAll").getResultList();
    }
}
