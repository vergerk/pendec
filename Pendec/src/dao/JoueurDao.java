/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Entite.Joueur;
import util.JpaUtil;

/**
 * DAO Joueur
 * @author jojod
 */
public class JoueurDao
{

    /**
     * Créer le joueur passé en paramètre
     * @param j
     */
    public void createJoueur(Joueur j)
    {
        JpaUtil.getEntityManager().persist(j);
    }

    /**
     * Met à jour le joueur passé en paramètre
     * @param j
     */
    public void updateJoueur(Joueur j)
    {
        JpaUtil.getEntityManager().merge(j);
    }

//    public Client findClientById (Long id)
//    {
//        Client c = JpaUtil.getEntityManager().find(Client.class,id);
//        return c;
//    }
//
//        public Client findClientByNom (String nom)
//    {
//        Client c ;
//        Query q= JpaUtil.getEntityManager().createQuery("Select c from Client c where c.nom = :varNom");
//        q.setParameter("varNom", nom);
//        c=(Client)q.getSingleResult();
//        return c;
//    }
//
//
//        public List<Client> findAllClient(){
//            Query q=JpaUtil.getEntityManager().createQuery("Select c from Client c");
//            return q.getResultList();
//        }
//
//        public void updateClient(Client c)
//        {
//            JpaUtil.getEntityManager().merge(c);
//        }
//
//        public void deleteClient(Client c)
//        {
//            JpaUtil.getEntityManager().remove(JpaUtil.getEntityManager().find(Client.class, c.getId()));
//        }
}
