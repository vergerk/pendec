/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;

/**
 *
 * @author jojod
 */
public class JpaUtil
{

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;
    private final static String persistenceUnitName = "PendecPU";

    /**
     * Initialise une nouvelle Factory pour l'EntityManager
     */
    public static void initEntityManagerFactory()
    {
        entityManagerFactory = Persistence.createEntityManagerFactory(persistenceUnitName);
    }

    /**
     * Créer un nouveau EntityManager
     */
    public static void createEntityManager()
    {
        if (entityManagerFactory != null) {
            try {
                entityManager = entityManagerFactory.createEntityManager();
            }
            catch (Exception ex) {
                JOptionPane.showMessageDialog(
                        null,
                        "Erreur lors de la création de l'entityManager",
                        "",
                        JOptionPane.QUESTION_MESSAGE
                );
            }
        }

    }

    /**
     * Détruit l'EntityManagerFactory
     */
    public static void destroyEntityManagerFactory()
    {
        if (entityManagerFactory != null && entityManagerFactory.isOpen()) {
            entityManagerFactory.close();
        }
    }

    /**
     * Ferme l'EntityManager
     */
    public static void closeEntityManager()
    {
        if (entityManager != null) {
            entityManager.close();
        }
    }

    /**
     * Retourne l'EntityManager
     * @return EntityManager statique de la classe
     */
    public static EntityManager getEntityManager()
    {
        return entityManager;
    }
}
