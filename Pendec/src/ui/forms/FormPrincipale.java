/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.forms;

import Entite.Joueur;
import Entite.Niveau;
import Entite.Theme;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import pendec.Communs;

/**
 * Fenêtre principale
 * @author VERGER Khevin
 */
public class FormPrincipale extends javax.swing.JFrame
{

    /**
     * Ouvre une nouvelle fenêtre principale
     */
    public FormPrincipale()
    {
        initComponents();
        Communs.initClasses();

        this.setLocationRelativeTo(null);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btn_lienProjet = new javax.swing.JButton();
        btn_nvellePartie = new javax.swing.JButton();
        btn_scores = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImages(null);
        setMaximumSize(new java.awt.Dimension(1000, 580));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setOpaque(false);

        jLabel1.setFont(new java.awt.Font("Viner Hand ITC", 0, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(240, 240, 240));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("PendEc");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        btn_lienProjet.setText("Voir le projet");
        btn_lienProjet.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btn_lienProjetActionPerformed(evt);
            }
        });

        btn_nvellePartie.setText("Nouvelle partie");
        btn_nvellePartie.setName(""); // NOI18N
        btn_nvellePartie.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btn_nvellePartieActionPerformed(evt);
            }
        });

        btn_scores.setText("Voir les scores");
        btn_scores.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btn_scoresActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_lienProjet, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btn_nvellePartie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
            .addComponent(btn_scores, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_nvellePartie)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_lienProjet)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_scores))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 30, 270, 210));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Ressources/formPrincipale_fond.png"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_lienProjetActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btn_lienProjetActionPerformed
    {//GEN-HEADEREND:event_btn_lienProjetActionPerformed
        try {
            //https://openclassrooms.com/forum/sujet/ouvrir-une-page-web-54152#message-6212532
            URI uri = URI.create("https://github.com/vergerk/pendec");
            Desktop.getDesktop().browse(uri);
        }
        catch (IOException ex) {
            Logger.getLogger(FormPrincipale.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btn_lienProjetActionPerformed

    private void btn_nvellePartieActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btn_nvellePartieActionPerformed
    {//GEN-HEADEREND:event_btn_nvellePartieActionPerformed
        // ouverture de la fenêtre de préparation

        // https://stackoverflow.com/questions/1481405/how-to-make-a-jframe-modal-in-swing-java
        FormPreparationJeu fpj = new FormPreparationJeu();

        final JDialog framePrep = new JDialog(this, fpj.getTitle(), true);
        fpj.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        framePrep.getContentPane().add(fpj.getPanel());
        framePrep.pack();
        framePrep.setLocationRelativeTo(null);
        framePrep.setVisible(true);

        fpj.dispose();

        //fpj.setVisible(true);
        if (fpj.isChoixValide()) {
            // on masque la fenêtre principale pour laisser place à celle du jeu
            this.setVisible(false);
            Joueur j = fpj.getJoueur();
            Theme t = fpj.getTheme();
            Niveau n = fpj.getNiveau();

            FormJeu fJeu = new FormJeu(j, t, n);

            final JDialog frameJeu = new JDialog(this, fJeu.getTitle(), true);
            fJeu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frameJeu.getContentPane().add(fJeu.getPanel());
            frameJeu.pack();
            frameJeu.setLocationRelativeTo(null);
            frameJeu.setVisible(true);

            System.out.println();

            // on affiche les scores si possible
            if (fJeu.isVoirScore()) {
                int scoreJeu = fJeu.getScore();

                FormScore fScore = new FormScore(fJeu.isAGagner(), scoreJeu, j, fJeu.getMot());

                final JDialog frameScore = new JDialog(this, fScore.getTitle(), true);
                fScore.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frameScore.getContentPane().add(fScore.getPanel());
                frameScore.pack();
                frameScore.setLocationRelativeTo(null);
                frameScore.setVisible(true);

                fJeu.dispose();
            }

            fJeu.dispose();

            // on remet la fenêtre principale une fois toutes les opérations finies
            this.setVisible(true);
        }
    }//GEN-LAST:event_btn_nvellePartieActionPerformed

    private void btn_scoresActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btn_scoresActionPerformed
    {//GEN-HEADEREND:event_btn_scoresActionPerformed
        /*
        le programme s'arrête à la fermeture de la fenêtre des scores

        FormScore fScore = new FormScore();
        fScore.setLocationRelativeTo(this);
        fScore.setVisible(true);
         */

        FormScore fScore = new FormScore();

        final JDialog frameScore = new JDialog(this, fScore.getTitle(), true);
        fScore.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frameScore.getContentPane().add(fScore.getPanel());
        frameScore.pack();
        frameScore.setLocationRelativeTo(null);
        frameScore.setVisible(true);
    }//GEN-LAST:event_btn_scoresActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormPrincipale.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new FormPrincipale().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_lienProjet;
    private javax.swing.JButton btn_nvellePartie;
    private javax.swing.JButton btn_scores;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
