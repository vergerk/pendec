/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.forms;

import Entite.Definition;
import Entite.Joueur;
import Entite.Niveau;
import Entite.Theme;
import java.awt.Dialog;
import java.util.Arrays;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import metier.logique.ServicesDefinition;
import pendec.Communs;
import util.JpaUtil;

/**
 * Fenêtre du jeu
 * @author VERGER Khevin
 */
public class FormJeu extends javax.swing.JFrame
{

    private final char caractCache = '*';

    private final int scoreGagnant = 200;
    private final int scorePerdant = -50;

    private int valPendu = 0;
    private int score = 0;
    private int nbImages = 0;

    private String caractAutorises = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private Definition mot = null;
    private Joueur joueur = null;

    private boolean voirScore = false;
    private boolean aGagner = false;
    private boolean jeuFini = false;

    private char[] motATrouver = null;
    private char[] motCache = null;

    public boolean isVoirScore()
    {
        return voirScore;
    }

    public int getScore()
    {
        return score;
    }

    public Definition getMot()
    {
        return mot;
    }

    public boolean isAGagner()
    {
        return aGagner;
    }

    /**
     * Ouvre une nouvelle fenêtre
     * @param j
     * @param t
     * @param n
     */
    public FormJeu(Joueur j, Theme t, Niveau n)
    {
        initComponents();

        this.setLocationRelativeTo(null);

        this.joueur = j;

        MajImagePendu();

        JpaUtil.initEntityManagerFactory();

        nbImages = Communs.getListImgPendu().size();

        mot = new ServicesDefinition().motAuHasard(n, t);
        String motTmp = mot.getMot().toLowerCase();

        // remplacement de "àâäèéêëîïôùûüÿçñ"
        motTmp = motTmp.replaceAll("[àâä]", "a");
        motTmp = motTmp.replaceAll("[èéêë]", "e");
        motTmp = motTmp.replaceAll("[îï]", "i");
        motTmp = motTmp.replaceAll("[ô]", "o");
        motTmp = motTmp.replaceAll("[ùûü]", "u");
        motTmp = motTmp.replaceAll("[ÿ]", "y");
        motTmp = motTmp.replaceAll("[ç]", "c");
        motTmp = motTmp.replaceAll("[ñ]", "ñ");

        motATrouver = motTmp.toUpperCase().toCharArray();
        motCache = new char[motATrouver.length];
        Arrays.fill(motCache, caractCache); // pour remplir un tableau avec un caractère

        for (int i = 0; i < motATrouver.length; i++) {
            char c = motATrouver[i];
            // on recherche les caractères non-alphabétiques pour les afficher par défaut
            if (!caractAutorises.contains(Character.toString(c))) {
                motCache[i] = c;
            }
        }
        MajMotAfficher(motCache);

        ftxt_saisie.setText("");
        jlb_score.setText("");
        /*
        Format format = new MessageFormat(caractAutorises);
        ftxt_saisie = new JFormattedTextField(format);
         */
        JpaUtil.destroyEntityManagerFactory();

        ftxt_saisie.requestFocus();
    }

    /**
     * Annonce la fin du jeu
     */
    private void FinJeu()
    {
        jeuFini = true;
        this.ftxt_saisie.setEditable(false);

        // on vérifie si le jeu contient encore des caractères à découvrir
        if (new String(motCache).indexOf(caractCache) != -1) {
            aGagner = false;
        }
        else {
            aGagner = true;
        }

        String msg = "";
        voirScore = true;

        int icone = 0;

        if (aGagner) {
            msg = "Bravo, t'as gagné. T'es vraiment trop fort !";
            icone = JOptionPane.INFORMATION_MESSAGE;
        }
        else {
            score = 0;
            jlb_score.setText(Integer.toString(this.score));
            MajMotAfficher(motATrouver);
            msg = "T'as gagné le droit de rejouer, puisqu'il fallait trouver\"" + mot.getMot() + "\"";
            icone = JOptionPane.ERROR_MESSAGE;
        }

        JOptionPane.showMessageDialog(
                null,
                msg,
                "",
                icone
        );

        FormMot f = new FormMot(mot);
        /*
        f.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
        f.setVisible(true);
         */

        final JDialog frame = new JDialog(this, f.getTitle(), true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(f.getPanel());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        f.dispose();
    }

    /**
     * Met à jour le score
     *@param score
     */
    private void MajScore(int score)
    {
        this.score += score;

        jlb_score.setText(Integer.toString(this.score));
    }

    /**
     * Met à jour le mot à afficher
     * @param motFormat
     */
    private void MajMotAfficher(char[] motFormat)
    {
        // on formatte le mot pour laisser des espaces entre les caractères
        String mot = FormattageMot(motFormat);

        this.txt_mot.setText(mot);
    }

    /**
     * Formattage le tableau de caractères entre un mot séparé par des espaces
     * @param mot
     * @return
     */
    private String FormattageMot(char[] mot)
    {
        String r = "";

        for (char c : mot) {
            r += c + " ";
        }

        return r.trim();
    }

    /**
     * Met à jour l'image du pendu
     */
    private void MajImagePendu()
    {
        lbImgPendu.setIcon(Communs.getListImgPendu().get(valPendu));
    }

    /**
     * Passe à la prochaine étape du pendu
     */
    private void ImagePenduSuivant()
    {
        valPendu++;

        if (valPendu < nbImages) {
            MajImagePendu();
        }

        if (valPendu == (nbImages - 1)) {
            this.FinJeu();
        }
    }

    /**
     * Réinitialise le pendu
     */
    private void ResetPendu()
    {
        valPendu = 0;
        MajImagePendu();
    }

    /**
     * Contrôle la présence du caractère passé en paramètre dans le mot et met à jour le pendu si nécessaire
     * @param c
     */
    private void ControlePresenceCaractere(char c)
    {
        int score = scorePerdant; // score par défaut quand on ne trouve pas le caractère
        c = Character.toUpperCase(c);
        boolean existe = false;

        // on boucle sur le mot à trouver
        for (int i = 0; i < motATrouver.length; i++) {
            // on vérifie uniquement sur les caractères non-découverts
            if (motCache[i] == caractCache && motATrouver[i] == c) {
                motCache[i] = c;
                score = scoreGagnant;
                existe = true;
            }
        }

        MajMotAfficher(motCache);
        MajScore(score);

        if (!existe) {
            this.ImagePenduSuivant();
        }

        // on vérifie si le jeu contient encore des caractères à découvrir
        if (!jeuFini && new String(motCache).indexOf(caractCache) == -1) {
            FinJeu();
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        lbImgPendu = new javax.swing.JLabel();
        txt_mot = new javax.swing.JTextField();
        ftxt_saisie = new javax.swing.JFormattedTextField();
        jlb_score = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addInputMethodListener(new java.awt.event.InputMethodListener()
        {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt)
            {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt)
            {
                formInputMethodTextChanged(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyPressed(java.awt.event.KeyEvent evt)
            {
                formKeyPressed(evt);
            }
        });

        lbImgPendu.setMaximumSize(new java.awt.Dimension(360, 512));
        lbImgPendu.setMinimumSize(new java.awt.Dimension(360, 512));
        lbImgPendu.setName(""); // NOI18N
        lbImgPendu.setPreferredSize(new java.awt.Dimension(360, 512));

        txt_mot.setEditable(false);
        txt_mot.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_mot.setText("txt_mot");

        ftxt_saisie.setColumns(1);
        ftxt_saisie.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        ftxt_saisie.setText("ftxt_saisie");
        ftxt_saisie.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        ftxt_saisie.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyPressed(java.awt.event.KeyEvent evt)
            {
                ftxt_saisieKeyPressed(evt);
            }
        });

        jlb_score.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jlb_score.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlb_score.setText("jlb_score");
        jlb_score.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbImgPendu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(txt_mot)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jlb_score, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(ftxt_saisie, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbImgPendu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_mot, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ftxt_saisie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlb_score))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formKeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_formKeyPressed
    {//GEN-HEADEREND:event_formKeyPressed

    }//GEN-LAST:event_formKeyPressed

    private void formInputMethodTextChanged(java.awt.event.InputMethodEvent evt)//GEN-FIRST:event_formInputMethodTextChanged
    {//GEN-HEADEREND:event_formInputMethodTextChanged

    }//GEN-LAST:event_formInputMethodTextChanged

    private void ftxt_saisieKeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_ftxt_saisieKeyPressed
    {//GEN-HEADEREND:event_ftxt_saisieKeyPressed
        char c = evt.getKeyChar();

        if (caractAutorises.contains(String.valueOf(c)) && !jeuFini) {
            if (this.ftxt_saisie.getText().length() > 0) {
                evt.consume();
                this.ftxt_saisie.setText("");
            }

            ControlePresenceCaractere(c);
        }
    }//GEN-LAST:event_ftxt_saisieKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        }
        catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormJeu.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        }
        catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormJeu.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        }
        catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormJeu.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        }
        catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormJeu.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                //new FormJeu(new Joueur()).setVisible(true);
            }
        });
    }

    public JRootPane getPanel()
    {
        return this.rootPane;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFormattedTextField ftxt_saisie;
    private javax.swing.JLabel jlb_score;
    private javax.swing.JLabel lbImgPendu;
    private javax.swing.JTextField txt_mot;
    // End of variables declaration//GEN-END:variables

}
